/**
 * @file slam.cpp
 * @author Sudharshan Suresh (suddhu@cmu.edu)
 * @date 2018-05-07
 * @brief Perform SLAM with landmarks looking-thru water
 */

#include <ConciseArgs>
#include <lcm/lcm-cpp.hpp>
#include "Frontend.hpp"

const std::string prefix("file://");
const std::string suffix("?speed=128");

int main(int argCount, char **argValues)
{
  std::clog << "[active-slam]\n";
  ConciseArgs parser(argCount, argValues);
  // std::string logfile, config_file("/home/suddhu/software/rpl-project-hauv/upward-cam/config/upward-slam.cfg");   // Lenovo X1 Carbon
  std::string logfile, config_file("/home/suddhu/projects/project-hauv/hauv-active-slam/config/active.cfg");       // Lenovo P51

  bool use_server = false;
  parser.add(logfile, "l", "logfile", "absolute or relative path to the log file to be used");
  parser.add(use_server, "s", "set this flag if using a botparam server is desired rather than a config file");
  parser.parse();	// parses all the variables from the command line terminal 

  Frontend frontend(prefix + logfile + suffix, config_file, use_server);	// goes into frontend constructor now 

  return 0;
}
