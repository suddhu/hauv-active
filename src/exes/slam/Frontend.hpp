/**
 * @file Frontend.hpp
 * @author Sudharshan Suresh (suddhu@cmu.edu)
 * @date 2018-05-07
 * @brief Frontend for SLAM framework 
 */

#ifndef FRONTEND_HPP
#define FRONTEND_HPP

#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <queue>
#include <mutex>
#include <thread>
#include <random>
#include <atomic>  //The atomic library provides components for fine-grained atomic operations allowing for lockless concurrent programming. E
#include <chrono>  // The chrono library, a flexible collection of types that track time with varying degrees of precision
#include <map>
#include <sys/stat.h> // create folders for images 
#include <sys/types.h>

#include <ConciseArgs>  // parsing arguments 
#include <lcm/lcm.h>
#include <lcm/lcm-cpp.hpp>
#include "lcmtypes/bot_core/image_t.hpp"
#include <bot_param/param_client.h> // param client
#include <hauv-didson/Huls3.hpp>    // Pose 

// CV
#include <opencv2/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

// PCL Viz
#include <pcl/common/time.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/point_types.h>

// Boost 
#include <boost/lexical_cast.hpp>

// upward-cam library
#include <hauv-active-slam/opencv-utils.hpp>
#include <hauv-active-slam/Image.hpp>
#include <hauv-active-slam/ImageFeatures.hpp>
#include <hauv-active-slam/ImagePairRegistration.hpp>
#include <hauv-active-slam/ImageSaliency.hpp>
#include <hauv-active-slam/Backend.hpp> // Backend

class Frontend
{
public:
  Frontend(std::string logfile, std::string config_file, bool use_server);

  ~Frontend(){};

  void lcmLoop();

  // Converts Master LCM msg to CV image (not needed)
  // rbuf : LCM buffer
  // chan: Master LCM channel.
  // msg : image_t LCM message
  void processProsilicaMasMessage(const lcm::ReceiveBuffer *rbuf,
                                  const std::string &chan,
                                  const bot_core::image_t *msg);

  // Converts Slave LCM msg to CV image (not needed)
  // rbuf : LCM buffer
  // chan: Slave LCM channel.
  // msg : image_t LCM message
  void processProsilicaSlvMessage(const lcm::ReceiveBuffer *rbuf,
                                  const std::string &chan,
                                  const bot_core::image_t *msg);

  // Callback on Master msg, calls processStereoPair in turn
  // rbuf : LCM buffer
  // chan: Master LCM channel.
  // msg : image_t LCM message
  void processMasStereo(const lcm::ReceiveBuffer *rbuf,
                        const std::string &chan,
                        const bot_core::image_t *msg);

  // Callback on Slave msg, calls processStereoPair in turn
  // rbuf : LCM buffer
  // chan: Slave LCM channel.
  // msg : image_t LCM message
  void processSlvStereo(const lcm::ReceiveBuffer *rbuf,
                        const std::string &chan,
                        const bot_core::image_t *msg);

  // Called with matching pair of stereo messages, 
  // mas_img : master (Image class)
  // slv_img : slave (Image class)
  void processStereoPair(Image mas_img, Image slv_img);

  // Interpolates HAUV pose based on timestamp 
  // msg : Image class with timstamp utime 
  // current_odom : output odometry of HAUV 
  void updatePose(Image msg, gtsam::Pose3 &current_odom);

  // // performs DA and landmark functions 
  // // current_pose : current HAUV pose 
  // void updateAssociations(gtsam::Pose3 current_pose);

  void writeCovariance();
 
  void displayStacked(cv::Mat img_mas, cv::Mat img_slv, int frame_count_);

  void writeTrajectory();

  void writeTrajLine(double timestamp, const gtsam::Pose3& p1, const gtsam::Pose3& p2, const gtsam::Pose3& p3);

  void writeMap(); 

  void writeMapLine(int id, const gtsam::Point3& p);

  void pause();

  void plotMatches();

  void refreshHistory();

  void saveImage(cv::Mat img_in);

  void computeSaliency(cv::Mat img);
  void pairwiseRegistration(cv::Mat img1, cv::Mat img2);

  // timing functions 
  void tic();
  void toc(std::string str);

  void ticInit();
  void tocInit(std::string str);

private:
  bool pause_, odometry_only_, use_server_, calibrate_, write_traj_, write_GT_, write_cov_, write_map_, save_image_, stereo_;
  int pose_num_;
  // float init_pix_err, track_pix_err, min_num_obs;
  std::atomic<bool> update_available_;  // for PCL visualization
  std::chrono::high_resolution_clock::time_point t0_;
  std::chrono::high_resolution_clock::time_point tStart_;

  BotParam* parameters_;
  double log_start_point;
  bool showImages, pauseImages, visualize, verbose, noSolve, isISAM,pre_proc; 

  unsigned seed_ = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine g_;
  double noise_to_add_;     // noise to add to odometry 
  std::normal_distribution<double> o_noise_;  // noise you can add to odometry 

  std::string prefix_, odom_traj_file_, slam_traj_file_, GT_traj_file_, cov_file_, map_file_, server_name_, config_file_, visMat_file_;
  std::ofstream ofs1A_, ofs1B_, ofs1C_, ofs2_, ofs3_;

  // LCM and threading 
  lcm::LCM lcm_node_;
  std::thread lcm_thread_;

  // master and slave stack 
  // StereoPairParams stpp; 
  std::stack<Image> mas_stack_, slv_stack_;
  std::vector<double> mas_timestamps_, slv_timestamps_;
  std::mutex stack_lock_;
  std::mutex optimize_lock; 

  // PCL Viz 
  std::shared_ptr<pcl::visualization::PCLVisualizer> viewer_;

  // Backend
  // std::shared_ptr<Backend> backend_;
  // BackendParams bp; 

  // pose variables 
  gtsam::Pose3 current_pose_, previous_pose_, initial_calib_, mas_pose_, slv_pose_, slv_ext_, mas_ext_;
  std::vector<gtsam::Pose3> odom_poses_, slam_poses_, noisy_odom_poses_;
  gtsam::Vector3 tv_p_;

  // Feat feat; // feature detector class 
  // Stats stats; 

  Huls3 huls_config_;    // config+state estimation
  std::queue<bot_core::image_t> msg_queue_;
  std::mutex queue_lock_;

  // history variables 
  cv::Mat current_mas, current_slv, prev_mas, prev_slv; 
  std::vector<cv::KeyPoint> kps_1, kps_2, prev_kps_1, prev_kps_2; 
  cv::Mat descriptors1, descriptors2, prev_descriptors1, prev_descriptors2;  
  // per_pose best;

  // hauv params
  gtsam::Vector5 mas_calib_, slv_calib_; // calibration variables for stereo pair   
  // std::vector<gtsam::Point3> landmarks;

  // landmark variables 
  // std::map<int,int> landmarks_count;
  // std::vector<gtsam::Point3> points3d; 
  // vector<int> oneCombo;
  // bool drop_flag = false; 
  // double ANMS_perc; 

  std::string image_path; 
  isam::Pose3d vehicle_global;
  // Data Association Class
  // std::shared_ptr<DataAssociation> assoc;

  double vehicle_start_time_, vehicle_stop_time_; 
  int frame_count_; 
  bool drop_flag = false; 



};

#endif // FRONTEND_HPP