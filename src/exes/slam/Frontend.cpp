/**
 * @file Frontend.cpp
 * @author Sudharshan Suresh (suddhu@cmu.edu)
 * @date 2018-05-07
 * @brief Frontend for SLAM framework 
 */

#include "Frontend.hpp"

const int dataset_size = 323; 

// static variables that need to be maintained across images
// bow vocabulary for an image sequence
cv::Mat bow_vocabulary_; // nDims x nWords
cv::Mat local_entropy_scores_; // nFrames x 1

Frontend::Frontend(std::string logfile, std::string config_file, bool use_server) :
  use_server_(use_server),
  pose_num_(-1),
  update_available_(false),
  g_(0),
  server_name_(""),
  config_file_(config_file),
  lcm_node_(logfile.c_str())
{
	// Load parameters from server or config file
	if (use_server_) {
		std::clog << "using parameter server: " << server_name_ << "\n";
		parameters_ = bot_param_new_from_named_server(lcm_node_.getUnderlyingLCM(), server_name_.c_str(), 1);
	} 
	else {
		std::clog << "using configuration file: " << config_file_ << "\n";
		parameters_ = bot_param_new_from_file(config_file_.c_str());
	}

	// misc parameters
	stereo_ = bot_param_get_boolean_or_fail(parameters_, "upward.slam.stereo");
	calibrate_ = bot_param_get_boolean_or_fail(parameters_, "upward.slam.calibrate");
	odometry_only_ = bot_param_get_boolean_or_fail(parameters_, "upward.slam.odometry_only");
	pause_ = bot_param_get_boolean_or_fail(parameters_, "upward.slam.pause");
	visualize = bot_param_get_boolean_or_fail(parameters_, "upward.slam.visualize");
	showImages = bot_param_get_boolean_or_fail(parameters_, "upward.slam.showImages");
	pauseImages = bot_param_get_boolean_or_fail(parameters_, "upward.slam.pauseImages");
	verbose = bot_param_get_boolean_or_fail(parameters_, "upward.slam.verbose");
	noSolve = bot_param_get_boolean_or_fail(parameters_, "upward.slam.noSolve");
	noise_to_add_ = bot_param_get_double_or_fail(parameters_, "upward.slam.noise_to_add");
	o_noise_ = std::normal_distribution<double>(0, noise_to_add_);
	prefix_ = bot_param_get_str_or_fail(parameters_, "upward.slam.prefix");
	write_traj_ = bot_param_get_boolean_or_fail(parameters_, "upward.slam.write_traj");
	write_cov_ = bot_param_get_boolean_or_fail(parameters_, "upward.slam.write_cov");
	write_map_ = bot_param_get_boolean_or_fail(parameters_, "upward.slam.write_map");
	save_image_ = bot_param_get_boolean_or_fail(parameters_, "upward.slam.save_image");

	log_start_point = bot_param_get_double_or_fail(parameters_, "upward.slam.log_start_point");

	if (log_start_point == 0)   std::cout<<"Starting log from beginning"<<std::endl;
	else                        std::cout<<"Starting log from "<<log_start_point<<std::endl;

	huls_config_ =  Huls3(lcm_node_, parameters_);  // get vehicle pose 

	// stereo LCM channels 
	std::string mas_image_channel = bot_param_get_str_or_fail(parameters_, "prosilica.master.channel");
	std::string slv_image_channel = bot_param_get_str_or_fail(parameters_, "prosilica.slave.channel");

//----------------------------  Backend Params --------------------------------------------------------//
 
 // to be added 

//----------------------------  StereoPairParams  --------------------------------------------------------//
	double mfl[2], mpp[2], sfl[2], spp[2], ic[6];

	// extrinsics 
	bot_param_get_double_array_or_fail(parameters_, "upward.slam.initial_calib", ic, 6);
	mas_ext_ = gtsam::Pose3(gtsam::Rot3::RzRyRx(ic[0], ic[1], ic[2]), gtsam::Point3(ic[3], ic[4], ic[5]));
	bot_param_get_double_array_or_fail(parameters_, "upward.slam.slv_ext", ic, 6);
	slv_ext_ = gtsam::Pose3(gtsam::Rot3::RzRyRx(ic[3], ic[4], ic[5]), gtsam::Point3(ic[0], ic[1], ic[2])).inverse();  // slave extrinsics 

	// intrinsics 
	bot_param_get_double_array_or_fail(parameters_, "prosilica.master.calib_water.fc", mfl, 2);
	bot_param_get_double_array_or_fail(parameters_, "prosilica.master.calib_water.cc", mpp, 2);
	bot_param_get_double_array_or_fail(parameters_, "prosilica.slave.calib_water.fc", sfl, 2);
	bot_param_get_double_array_or_fail(parameters_, "prosilica.slave.calib_water.cc", spp, 2);


// to be added
	mas_calib_ = (gtsam::Vector(5) <<  mfl[0], mfl[1],  0.0, mpp[0], mpp[1]).finished();
	slv_calib_ = (gtsam::Vector(5) << sfl[0], sfl[1],  0.0, spp[0], spp[1]).finished();

//----------------------------  featureDetector  --------------------------------------------------------//

// to be added

//----------------------------  miscellaneous  --------------------------------------------------------//
	// Setup file writing
	size_t pos1 = logfile.rfind("/");
	size_t pos2 = logfile.rfind("?");
	std::string file_pre = logfile.substr(pos1, pos2 - pos1);

	odom_traj_file_ = std::string(prefix_ + file_pre + "-odom.txt");  // odometry readings (noisy)
	slam_traj_file_ = std::string(prefix_ + file_pre + "-slam.txt");  // slam readings (optimized)
	GT_traj_file_ = std::string(prefix_ + file_pre + "-GT.txt");    // ground truth (HAUV)
	cov_file_  = std::string(prefix_ + file_pre + "-cov.txt");      // covariance readings (isam only)
	map_file_ = std::string(prefix_ + file_pre + "-map.txt");       // landmark locations

	if (write_traj_) {
		ofs1A_.open(odom_traj_file_, std::ofstream::trunc | std::ofstream::out);
		ofs1B_.open(slam_traj_file_, std::ofstream::trunc | std::ofstream::out);
		ofs1C_.open(GT_traj_file_, std::ofstream::trunc | std::ofstream::out);
	}

	if (write_cov_)  ofs2_.open(cov_file_,  std::ofstream::trunc | std::ofstream::out);
	if (write_map_)  ofs3_.open(map_file_,  std::ofstream::trunc | std::ofstream::out);

	if (save_image_) { 
		image_path = prefix_  + "/images/" + file_pre;
		const int dir_err = mkdir(image_path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
		if (-1 == dir_err) {
		    printf("Error creating images directory!n");
		    exit(1);
		}
	}
//----------------------------  Data Association  --------------------------------------------------------//
// to be added
//----------------------------  Setup LCM  --------------------------------------------------------//
	if (!lcm_node_.good()) { std::cout << "LCM not good\n"; exit(1); }
	lcm_node_.subscribe(mas_image_channel, &Frontend::processMasStereo, this);
	lcm_node_.subscribe(slv_image_channel, &Frontend::processSlvStereo, this);
	lcm_thread_ = std::thread(&Frontend::lcmLoop, this); // thread 
//----------------------------  Visualize  --------------------------------------------------------//

	if (visualize) {
		viewer_ = std::shared_ptr<pcl::visualization::PCLVisualizer>(new pcl::visualization::PCLVisualizer());
		viewer_->setBackgroundColor(0, 0, 0);
		viewer_->setWindowName ("HAUV Visualization"); 
		viewer_->loadCameraParameters("hauv-active-slam/src/hauv-active-slam/params.cam");  // do I need to change this? 

		// Visualizer main loop
		while (!viewer_->wasStopped()) {
			viewer_->spinOnce(2);
			boost::this_thread::sleep(boost::posix_time::microseconds(1));

			if (update_available_.load()) {

				update_available_.store(false);

				// add coordinates 
				Eigen::Affine3f affine_v(current_pose_.matrix().cast<float>());
				Eigen::Affine3f affine_m(mas_pose_.matrix().cast<float>());

				viewer_->removeCoordinateSystem("vehicle");
				viewer_->removeCoordinateSystem("master");
				viewer_->addCoordinateSystem(0.5, affine_v, "vehicle");
				viewer_->addCoordinateSystem(0.3, affine_m, "master");

				Eigen::Affine3f affine_s((slv_pose_).matrix().cast<float>());
				viewer_->removeCoordinateSystem("slave");
				viewer_->addCoordinateSystem(0.3, affine_s, "slave");

			    try {
					for (int i = 1; i < pose_num_ - 1; i++) {
						std::string number(boost::lexical_cast<std::string>(i)); 
						viewer_->removeShape("trajectory2" + number);

						// Plot GT trajectory - green
						viewer_->addLine(pcl::PointXYZ(odom_poses_[i-1].translation().x(),odom_poses_[i-1].translation().y(),odom_poses_[i-1].translation().z()),
						               pcl::PointXYZ(odom_poses_[i].translation().x(),odom_poses_[i].translation().y(),odom_poses_[i].translation().z()), 0, 1, 0, "trajectory2" + number);

						viewer_->setShapeRenderingProperties (pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 4, "trajectory2" + number);
						}
			    	}
			    catch (gtsam::ValuesKeyDoesNotExist &e) {
					std::cout<<"skipping plotting here!!\n"; 
			    	}
	    		viewer_->removeAllPointClouds();
			}
		}
	}
	lcm_thread_.join();
}

void Frontend::lcmLoop()
{
  while (lcm_node_.handleTimeout(1000) > 0) {}
  std::cout<<vehicle_stop_time_<<" seconds for total vehicle run time!\n";
  std::cout << "Done with LCM! Close PCL window.\n";
  return;
}


// interpolates HAUV pose and adds to graph
void Frontend::updatePose(Image msg, gtsam::Pose3 &current_odom)
{
  // get time
  double query_time = msg.getUtime(); 
  double latest_time = huls_config_.LatestTime();
  HauvConfigState config_state;

  if (query_time > latest_time) {
    std::clog << "State estimate not yet available; sleeping for 200ms\n";
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
  } 

  if (!huls_config_.GetState(query_time, config_state)) {
    std::clog<< "State estimate (still) not available; dropping message!\n";
    drop_flag = true; 
  }

  // to prevent lack of state estimate! 
  if (!drop_flag)
    vehicle_global = config_state.vehicle;

  gtsam::Vector3 tv(vehicle_global.x(), vehicle_global.y(), vehicle_global.z());
  Eigen::Quaterniond q = vehicle_global.rot().quaternion();
  gtsam::Rot3 rot(gtsam::Quaternion(q.w(), q.x(), q.y(), q.z()));
  
  current_odom = gtsam::Pose3(rot, gtsam::Point3(tv));

  gtsam::Pose3 current_odom_orig = current_odom;

  if (pose_num_ > 0)
  {
    double dt =  mas_timestamps_.at(mas_timestamps_.size() - 1) -  mas_timestamps_.at(mas_timestamps_.size() - 2);
    
    // to prevent random jumps? 
    gtsam::Vector3 vel = (tv - tv_p_) / dt;
    if (fabs(vel(0)) > 0.3) tv(0) = tv_p_(0);
    if (fabs(vel(1)) > 0.3) tv(1) = tv_p_(1);
    if (fabs(vel(2)) > 0.3) tv(2) = tv_p_(2);

    // add noise 
    if (noise_to_add_ > 0.0001)
    {
      // // Add noise in all directions
      // rot = gtsam::Rot3::RzRyRx(rot.roll() + o_noise_(g_), rot.pitch() + o_noise_(g_), rot.yaw() + o_noise_(g_));
      // tv(0) += o_noise_(g_);
      // tv(1) += o_noise_(g_);
      // tv(2) += o_noise_(g_);

      // Add noise in XYH directions
      rot = gtsam::Rot3::RzRyRx(rot.roll(), rot.pitch(), rot.yaw() + o_noise_(g_));
      tv(0) += o_noise_(g_);
      tv(1) += o_noise_(g_);
    }

    current_odom = gtsam::Pose3(rot, gtsam::Point3(tv));
    gtsam::Pose3 odom_diff = odom_poses_.back().transform_pose_to(current_odom);

    current_odom.print("current odom: "); 
    noisy_odom_poses_.push_back(noisy_odom_poses_.back()*odom_diff);  // noisy odometry for plot

    // std::cout<<"added as pose "<<pose_num_<<std::endl;
    // backend_->addNewPose(mas_timestamps_.back(), current_odom, odom_diff);  // subsequent poses 
  } 
  else
  {
    noisy_odom_poses_.push_back(current_odom);
    // backend_->addFirstPose(mas_timestamps_.back(), current_odom); // first pose 
  }

  odom_poses_.push_back(current_odom_orig); // ground truth odometry for plot 
  tv_p_ = current_odom_orig.translation().vector();
  return; 
}


void Frontend::saveImage(cv::Mat img_in) {
	cv::Mat img_gray8;
	img_in.convertTo(img_gray8, CV_8U, 0.00390625);
	std::stringstream ss_file;
	std::string dir = std::string(image_path + "/");
	ss_file.str(""); ss_file << dir << std::setfill('0') << std::setw(4) << frame_count_ << ".jpg";
	cv::imwrite(ss_file.str(), img_gray8);
	std::cout << "Written Image File : " << ss_file.str() << std::endl; 
}

// convert and display master image 
void Frontend::processProsilicaMasMessage(const lcm::ReceiveBuffer *rbuf,
                                          const std::string &chan,
                                          const bot_core::image_t *msg)
{
  (void) rbuf; (void) chan;
  std::vector< uint8_t > data2 = msg->data;

  const uint16_t *pixel16 = (uint16_t *) msg->data.data();
  uint8_t *pixel8 = data2.data();
  for (int i=0; i<(msg->height*msg->width); i++, pixel16++, pixel8++)
    *pixel8 = (*pixel16) >> 8;

  cv::Mat mat(msg->height, msg->width, CV_8UC1, data2.data());
  cv::imshow("Master", mat);
  cv::waitKey(0);
}

// convert and display slave image 
void Frontend::processProsilicaSlvMessage(const lcm::ReceiveBuffer *rbuf,
                                          const std::string &chan,
                                          const bot_core::image_t *msg)
{
  (void) rbuf; (void) chan;
  std::vector< uint8_t > data2 = msg->data;

  const uint16_t *pixel16 = (uint16_t *) msg->data.data();
  uint8_t *pixel8 = data2.data();
  for (int i=0; i<(msg->height*msg->width); i++, pixel16++, pixel8++)
    *pixel8 = (*pixel16) >> 8;

  cv::Mat mat(msg->height, msg->width, CV_8UC1, data2.data());
  cv::imshow("Slave", mat);
  cv::waitKey(0);
}


// process master camera data 
void Frontend::processMasStereo(const lcm::ReceiveBuffer *rbuf,
                                const std::string &chan,
                                const bot_core::image_t *msg)
{ 
  // start from good point in the log
  if (msg->utime < log_start_point)   return;

  (void) rbuf; (void) chan;
  stack_lock_.lock();
  bool matched = false;

  Image latest_master(msg, 0);

  // check if something is in the slave stack
  if (slv_stack_.size() > 0) 
  {
    Image slave_front_ = slv_stack_.top();
    double mas_time = msg->utime/ 1E6;
    double slv_time = slv_stack_.top().getUtime();
    if (fabs(mas_time - slv_time) < 1E-2)
    {
      processStereoPair(latest_master, slave_front_);
      matched = true;
      slv_stack_.pop();
    }
  }
  
  if (!matched) mas_stack_.push(latest_master);
  stack_lock_.unlock();
  return;
}

// process slv camera data 
void Frontend::processSlvStereo(const lcm::ReceiveBuffer *rbuf,
                                const std::string &chan,
                                const bot_core::image_t *msg)
{
  // start from good point in the log
  if (msg->utime < log_start_point)   return;

  (void) rbuf; (void) chan;
  stack_lock_.lock();
  bool matched = false;

  Image latest_slave(msg, 1);

  // check if something is in the master stack
  if (mas_stack_.size() > 0) 
  {
    Image master_front_ = mas_stack_.top(); 
    double mas_time = mas_stack_.top().getUtime();
    double slv_time = msg->utime/ 1E6;
    std::cout<<mas_time<<" "<<slv_time<<std::endl;
    if (fabs(mas_time - slv_time) < 1E-2)
    {
      processStereoPair(master_front_,latest_slave);
      matched = true;
      mas_stack_.pop();
    }
  }
  
  if (!matched) slv_stack_.push(latest_slave);
  stack_lock_.unlock();
  return;
}

// process when mas + slv recieved 
void Frontend::processStereoPair(Image mas_img, Image slv_img) {
	pose_num_++;
	mas_timestamps_.push_back(mas_img.getUtime());
	slv_timestamps_.push_back(slv_img.getUtime());

	current_mas = mas_img.getRectifiedImage(); 
	current_slv = slv_img.getRectifiedImage(); 

	if (save_image_)    saveImage(current_mas);       // save Images 

	gtsam::Pose3 current_pose; 

	updatePose(mas_img, current_pose);  // update pose and add to graph

	// do stuff here

	if (pose_num_!=0 && showImages) {
		// code to show output
		pairwiseRegistration(current_mas, prev_mas);
		computeSaliency(current_mas);
	}

	if (pose_num_ == 0)		vehicle_start_time_ = mas_img.getUtime();
	
	else 					vehicle_stop_time_ = mas_img.getUtime() - vehicle_start_time_;
  
	previous_pose_ = current_pose; 

	mas_pose_ = current_pose*mas_ext_;
	slv_pose_ = mas_pose_*slv_ext_; 

	std::this_thread::sleep_for(std::chrono::microseconds(2));

	std::cout<<std::endl<<std::endl;
	refreshHistory();   // maintain history
	update_available_.store(true);    // green light for visualizer
  	return;
}

void Frontend::computeSaliency(cv::Mat img) {
  cv::Mat img_filt;
  img_filt = cvutils::applyGaussianBlur(img, 3);

  cv::imshow("img_filt", img_filt );
  cv::waitKey(0);  // Extract feature descriptors

  ImageFeatures featObj;
  featObj.setInputImage(img_filt);
  featObj.computeSURFFeatures();
  cv::Mat feat_descriptors = featObj.getSURFDescriptor();
  cv::Mat img_keypoints = featObj.getImageWithSURFKeypoints();

  // Compute Saliency from extracted feature descriptors
  ImageSaliency salObj;
  salObj.initBoWVocabulary(bow_vocabulary_);
  salObj.setInputFeatureDescriptors(feat_descriptors);
  float local_entropy_score = salObj.getLocalEntropyScore();
  cv::Mat img_bow_hist = salObj.getBoWHistogramImage();
  bow_vocabulary_ = salObj.getUpdatedBoWVocabulary();

  if (local_entropy_scores_.empty() == 1) {
    local_entropy_scores_ = cv::Mat(1, 1, CV_32FC1, local_entropy_score);
  }
  else {
    local_entropy_scores_.push_back(cv::Mat(1, 1, CV_32FC1, local_entropy_score));
  }

  cv::Mat local_saliency_scores = local_entropy_scores_ / std::log2(bow_vocabulary_.rows);
  std::cout << "No. of words in BoW vocabulary : " << bow_vocabulary_.rows << std::endl; 

  // DEBUG files
  std::string dir = "/home/suddhu/projects/project-hauv/hauv-active-slam/debug";
  std::stringstream ss_file;

  // Write saliency scores to file
  ss_file.str(""); ss_file << dir << "/local_saliency_scores.yml";
  cv::FileStorage file(ss_file.str(), cv::FileStorage::WRITE);
  file << "local_saliency_scores" << local_saliency_scores;

  // Write Image with drawn Keypoints to file
  ss_file.str(""); ss_file << dir << std::setfill('0') << "/img_rect_" << std::setw(4) << pose_num_ << ".jpg";
  cv::imwrite(ss_file.str(), img_keypoints);
  ss_file.str(""); ss_file << dir << std::setfill('0') << "/img_bowhist_" << std::setw(4) << pose_num_ << ".jpg";
  cv::imwrite(ss_file.str(), img_bow_hist);
}

void Frontend::pairwiseRegistration(cv::Mat img1, cv::Mat img2) {

	// CLAHE filter doesn't do much good - re-evaluate later
	// img1 = cvutils::applyCLAHEFilter(img1);
	// img2 = cvutils::applyCLAHEFilter(img2);

	img1 = cvutils::applyGaussianBlur(img1, 3);
	img2 = cvutils::applyGaussianBlur(img2, 3);

	cv::Mat descriptors1, descriptors2;
	std::vector<cv::KeyPoint> kps_1, kps_2;

	// // Compute 2D SURF features for both pose estimation and saliency tests 
	ImageFeatures featObj;
	featObj.setInputImage(img1);
	featObj.computeSURFFeatures();
	descriptors1 = featObj.getSURFDescriptor();
	kps_1 = featObj.getSURFKeypoints();

	featObj.setInputImage(img2);
	featObj.computeSURFFeatures();
	descriptors2 = featObj.getSURFDescriptor();
	kps_2 = featObj.getSURFKeypoints();

	ImagePairRegistration pairReg(img1, img2);
	pairReg.setFeatureDescriptors(descriptors1, descriptors2);
	pairReg.setFeatureKeypoints(kps_1, kps_2);
	pairReg.matchFeatureDescriptorsGlobally();
	pairReg.estimatePoseFromMatches();
}

// update keypoints and descriptors across frames 
void Frontend::refreshHistory() {
  current_mas.copyTo(prev_mas);
  current_slv.copyTo(prev_slv);
  prev_kps_1 = kps_1; prev_kps_2 = kps_2;
  prev_descriptors1 = descriptors1; prev_descriptors2 = descriptors2;
  kps_1.clear(); kps_2.clear(); 
  return;
}
