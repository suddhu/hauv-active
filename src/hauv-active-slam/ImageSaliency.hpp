/**
 * @file ImageSaliency.hpp
 * @author Paloma Sodhi (psodhi@andrew.cmu.edu)
 * @date 2017-10-26
 * @brief A class 
 */

#pragma once

#include <cstdint>
#include <iostream>
#include <vector>

#include "lcmtypes/bot_core/image_t.hpp"

#include "opencv-utils.hpp"
#include "opencv2/xfeatures2d.hpp"
#include <opencv2/features2d/features2d.hpp>

/*! \class ImageSaliency ImageSaliency.hpp "hauv-vision/ImageSaliency.hpp"
 *  \brief Brief description.
 *
 */

class ImageSaliency
{

public:
	/*!
	  \brief Default constructor.
	 */
	ImageSaliency();

	/*!
	  \brief Default destructor.
	 */
	~ImageSaliency(){
		cv::destroyAllWindows();
	}

	// void setInputImage(cv::Mat);
	void initBoWVocabulary(cv::Mat);
	void setInputFeatureDescriptors(cv::Mat); // set feature descriptors of single input image	
	cv::Mat getBoWHistogramImage();
	void computeBoWHistogramImage(cv::Mat);
	float getLocalEntropyScore();
	unsigned int bestBoWMatch(cv::Mat);

	cv::Mat getUpdatedBoWVocabulary();	

private:
    cv::Mat img_in_;
    cv::Mat feat_descriptor_; // nDims x nKeypts
    cv::Mat img_bow_hist_;
	float local_saliency_score_;
	float global_saliency_score_;   

	cv::Mat bow_vocabulary_; // num_dim x num_vocabwords
	// cv::Mat bow_histogram_; // num_dim x num_keypts
	float bow_thresh_;

	bool vis_flag_;
}; 