/**
 * @file opencv-utils.cpp
 * @author Paloma Sodhi (psodhi@andrew,cnu.edu)
 * @date 2017-10-24
 * @brief A set of utility functions to work with the OpenCV library
 * (implementation file)
 */

#include "opencv-utils.hpp"

namespace cvutils { 

cv::Mat applyCLAHEFilter(cv::Mat img_in) {

  cv::Mat img_out;

  double clipLimit = 1;
  cv::Size tileGridSize = cv::Size(8,8); 

  cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE();
  clahe->setClipLimit(clipLimit);
  clahe->setTilesGridSize(tileGridSize);  
  clahe->apply(img_in, img_out);

  return img_out;
}

std::string imtype2str(int type) {
  std::string str;

  uchar depth = type & CV_MAT_DEPTH_MASK;
  uchar chans = 1 + (type >> CV_CN_SHIFT);

  switch ( depth ) {
    case CV_8U:  str = "8U"; break;
    case CV_8S:  str = "8S"; break;
    case CV_16U: str = "16U"; break;
    case CV_16S: str = "16S"; break;
    case CV_32S: str = "32S"; break;
    case CV_32F: str = "32F"; break;
    case CV_64F: str = "64F"; break;
    default:     str = "User"; break;
  }

  str += "C";
  str += (chans+'0');

  return str;
}

cv::Mat applyGaussianBlur(cv::Mat img_in, int kernel_size) {
  cv::Mat img_out;
  cv::blur(img_in, img_out, cv::Size(kernel_size, kernel_size));
  return img_out;
}


void visualize(cv::Mat img_in, std::string window_name) {
  // cv::namedWindow("vis_window", cv::WINDOW_AUTOSIZE);
  cv::imshow(window_name, img_in);       
  cv::waitKey(3);                                       
}

}