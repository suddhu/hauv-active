/**
* @file opencv-utils.hpp
* @author Paloma Sodhi (psodhi@andrew.cmu.edu)
* @date 2017-10-25
* @brief A set of utility functions to work with the OpenCV library.
* (header file)
*/

#ifndef OPENCV_UTILS_HPP
#define OPENCV_UTILS_HPP

#include <iostream>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

namespace cvutils {

cv::Mat applyCLAHEFilter(cv::Mat);
cv::Mat applyGaussianBlur(cv::Mat, int);
std::string imtype2str(int);

void visualize(cv::Mat, std::string window_name);

}

#endif // OPENCV_UTILS_HPP
