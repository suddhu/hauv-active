/**
 * @file Image.hpp
 * @author Paloma Sodhi (psodhi@andrew.cmu.edu)
 * @date 2017-10-24
 * @brief A class to handle camera lcm messages from the HAUV.
 */

#ifndef IMAGE_HPP_ 
#define IMAGE_HPP_

#include <cstdint>
#include <iostream>
#include <vector>

#include "lcmtypes/bot_core/image_t.hpp"

#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>

#include <stdio.h>

/*! \class Image Image.hpp "hauv-vision/Image.hpp"
 *  \brief Brief description.
 *
 * 
 */

class Image
{

public:
	/*!
	  \brief Default constructor.
	 */
	Image(){};

	/*!
	  \brief Constructor from an LCM message
	  \param [in] message The camera image_t message.
	 */
	Image(const bot_core::image_t *message, const bool cam_id);

	/*!
	  \brief Default destructor.
	 */
	~Image(){
		cv::destroyAllWindows();
	};

	cv::Mat getRawImage();
	cv::Mat getRectifiedImage();
	double 	getUtime();

	void setFrameID(int);
	cv::Mat rectifyImage();

private:
	double utime_;
    int frame_id_;
    int cam_id_; // 0 : PROSILICA_STEREOMAS, 1 : PROSILICA_STEREOSLV

    unsigned int img_width_, img_height_;
    cv::Mat img_raw_;
    cv::Mat img_rect_;
};

#endif