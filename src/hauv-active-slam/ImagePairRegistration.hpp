/**
 * @file ImagePairRegistration.hpp
 * @author Paloma Sodhi (psodhi@andrew.cmu.edu)
 * @date 2017-11-09
 * @brief A class 
 */

#ifndef IMAGEPAIRREGISTRATION_HPP_ 
#define IMAGEPAIRREGISTRATION_HPP_

#include <cstdint>
#include <iostream>
#include <vector>

#include "lcmtypes/bot_core/image_t.hpp"

#include "opencv-utils.hpp"
#include "opencv2/xfeatures2d.hpp"
#include <opencv2/features2d/features2d.hpp>



/*! \class ImagePairRegistration ImagePairRegistration.hpp "hauv-vision/ImagePairRegistration.hpp"
 *  \brief Brief description.
 *
 * 
 */

class ImagePairRegistration
{

public:
	/*!
	  \brief Default constructor.
	 */
	ImagePairRegistration();

	/*!
	  \brief Constructor from a cv::Mat image
	  \param [in] message Pair of cv::Mat images img1, img2 where img2 is to be registered against img1
	 */
	ImagePairRegistration(cv::Mat, cv::Mat);

	/*!
	  \brief Default destructor.
	 */
	~ImagePairRegistration(){
		cv::destroyAllWindows();
	}

	void setFeatureDescriptors(cv::Mat, cv::Mat);
	void setFeatureKeypoints(std::vector<cv::KeyPoint>, std::vector<cv::KeyPoint>);
	void visualizeFeatureMatches(std::vector<cv::DMatch>);
	void matchFeatureDescriptorsGlobally();
	void estimatePoseFromMatches();

private:
	cv::Mat img_1_, img_2_;

	cv::Mat descriptors_1_, descriptors_2_;
	std::vector<cv::KeyPoint> keypoints_1_, keypoints_2_;
	std::vector<cv::DMatch> matches_global_, matches_global_robust_;

	cv::Mat fundamental_matrix_;
	cv::Mat essential_matrix_;
	cv::Mat R_, t_;
};

#endif