#include "ImageSaliency.hpp"

ImageSaliency::ImageSaliency(){
  bow_thresh_ = 0.65;
}

void ImageSaliency::initBoWVocabulary(cv::Mat bow_vocabulary) {
  bow_vocabulary_ = bow_vocabulary;
}

cv::Mat ImageSaliency::getUpdatedBoWVocabulary() {
  return bow_vocabulary_;
}

void ImageSaliency::setInputFeatureDescriptors(cv::Mat feat_descriptor) {
  feat_descriptor_ = feat_descriptor; // nKeypts (rows) x nDims (cols)
}

cv::Mat ImageSaliency::getBoWHistogramImage() {
  return img_bow_hist_;
}

void ImageSaliency::computeBoWHistogramImage(cv::Mat bow_histogram) {

  int hist_size = 75;
  float range[] = { 0, 75 } ;
  const float* hist_range = { range };
  bool uniform = true;
  bool accumulate = false;

  int hist_w = 512; int hist_h = 400;
  int bin_w = cvRound((double) hist_w/hist_size);
  cv::Mat img_hist_bow( hist_h, hist_w, CV_8UC1, cv::Scalar(255,255,255) );

  cv::Mat bow_histogram_hist;
  cv::calcHist(&bow_histogram, 1, 0, cv::Mat(), bow_histogram_hist, 1, &hist_size, &hist_range, uniform, accumulate);
  cv::normalize(bow_histogram_hist, bow_histogram_hist, 0, 75, cv::NORM_MINMAX, -1, cv::Mat());

  for( int i = 1; i < hist_size; i++ )
    cv::line(img_hist_bow, cv::Point( bin_w*(i-1), hist_h - cvRound(bow_histogram_hist.at<float>(i-1)) ),
              cv::Point( bin_w*(i), hist_h - cvRound(bow_histogram_hist.at<float>(i)) ), cv::Scalar( 0, 0, 100), 2, 8, 0);

  img_bow_hist_ = img_hist_bow;

  // cv::namedWindow("img_hist_bow", CV_WINDOW_AUTOSIZE );
  // cv::imshow("img_hist_bow", img_hist_bow );
  // cv::waitKey(0);

}

float ImageSaliency::getLocalEntropyScore() {
  cv::Mat query_descriptors = feat_descriptor_;
  int num_descriptors = query_descriptors.rows;
  int num_dims = query_descriptors.cols;

  cv::Mat bow_histogram = cv::Mat::zeros(bow_vocabulary_.rows, 1, CV_8UC1); // num_vocabwords x 1

  // Compute Bag-of-Words Histogram
  for (int i = 0; i < num_descriptors; i++) {

    int word_idx = bestBoWMatch(query_descriptors.row(i));

    if (word_idx < bow_histogram.rows)
      bow_histogram.row(word_idx) = bow_histogram.row(word_idx) + 1;  // increase occurance of existing BoW word
    else if (word_idx == bow_histogram.rows)
      cv::vconcat(bow_histogram, cv::Mat::ones(1,1,CV_8UC1), bow_histogram);  // add new word to vocabulary
    else
      std::cout << "ERROR: Unexpected Additions to BoW Vocabulary" << std::endl;  // wut
  }

  // Compute Histogram Image
  computeBoWHistogramImage(bow_histogram);

  // Compute Local Saliency
  cv::Mat prob_histogram;
  bow_histogram.convertTo(prob_histogram, CV_32FC1);
  prob_histogram = prob_histogram / num_descriptors;
  float local_entropy_score = 0.0;
  for (int k = 0; k < prob_histogram.rows; k++) {
    float prob_word_k = prob_histogram.at<float>(k,0);
    if (prob_word_k > 1e-6)
      local_entropy_score = local_entropy_score - prob_word_k*( std::log2(prob_word_k) );
  }

  // This value changes for previous image frames as total words in bag-of-words vocabulary changes
  // float local_saliency = local_entropy / std::log2(bow_vocabulary_.rows);
  // std::cout << "Local Saliency Value : " << local_saliency << std::endl;

  return local_entropy_score;
}

unsigned int ImageSaliency::bestBoWMatch(cv::Mat query_descriptor) {

  /** Dimensions
  query_descriptors : nKeypts (rows) x nDims (cols)  
  query_descriptor : 1 (rows) x nDims (cols)
  bow_vocabulary_ : nWords (rows) x nDims (cols)
  **/

  // If bow_vocabulary_ database is empty, add the first feature descriptor to the vocabulary  
  if (bow_vocabulary_.empty() == 1) {
    bow_vocabulary_ = query_descriptor;
    return 0;
  }

  // find best vocabulary match in online bow_vocabulary_ database
  cv::Ptr<cv::DescriptorMatcher> matcher = cv::DescriptorMatcher::create("BruteForce"); // BruteForce : L2Norm
  std::vector<cv::DMatch> matches;
  matcher->match(query_descriptor, bow_vocabulary_, matches, cv::noArray());
  float dot_prod = query_descriptor.dot(bow_vocabulary_.row(matches[0].trainIdx));

  // update online bow_vocabulary_ database if new descriptor is further than bow_thresh_ from nearest word
  if (std::abs(dot_prod) < bow_thresh_) {
    cv::vconcat(bow_vocabulary_, query_descriptor, bow_vocabulary_);
    return (bow_vocabulary_.rows-1); // idx of last word in bow_vocabulary_
  }
  else {
    return matches[0].trainIdx;
  }

}

