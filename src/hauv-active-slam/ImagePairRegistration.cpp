#include "ImagePairRegistration.hpp"

ImagePairRegistration::ImagePairRegistration(cv::Mat img_1, cv::Mat img_2) {
	img_1_ = img_1;
	img_2_ = img_2;
}

void ImagePairRegistration::setFeatureDescriptors(cv::Mat descriptors_1, cv::Mat descriptors_2) {
	descriptors_1_ = descriptors_1;
	descriptors_2_ = descriptors_2;
}

void ImagePairRegistration::setFeatureKeypoints(std::vector<cv::KeyPoint> keypoints_1, std::vector<cv::KeyPoint> keypoints_2) {
	keypoints_1_ = keypoints_1;
	keypoints_2_ = keypoints_2;
}

void ImagePairRegistration::visualizeFeatureMatches(std::vector<cv::DMatch> matches_vis) {
	cv::Mat img_matches;
	cv::drawMatches(img_1_, keypoints_1_, img_2_, keypoints_2_,
	             matches_vis, img_matches, cv::Scalar::all(-1), cv::Scalar::all(-1),
	             std::vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
	cv::imshow("img_matches", img_matches );
	cv::waitKey(1);
}

void ImagePairRegistration::matchFeatureDescriptorsGlobally() {

	// switched to using https://stackoverflow.com/a/17977207
	std::vector<std::vector<cv::DMatch>> matches;
	cv::BFMatcher matcher;
	// matches_global_.clear();
	matcher.knnMatch(descriptors_1_, descriptors_2_, matches, 2);  // Find two nearest matches
	for (int i = 0; i < matches.size(); ++i)
	{
	    const float ratio = 0.8; // As in Lowe's paper; can be tuned
	    if (matches[i][0].distance < ratio * matches[i][1].distance)
	    {
	        matches_global_.push_back(matches[i][0]);
	    }
	}

}

void ImagePairRegistration::estimatePoseFromMatches() {

	if (matches_global_.size() > 5) {
		std::vector<cv::Point2f> img1_pts, img2_pts;
		for(unsigned int i = 0; i < matches_global_.size(); i++) {
			img1_pts.push_back(keypoints_1_[matches_global_[i].queryIdx].pt);
			img2_pts.push_back(keypoints_2_[matches_global_[i].trainIdx].pt);
		}

		// TODO : Set camera intrinsics from outside the class
		double fx = 843.13619593, fy = 840.04059281; // PROSILICA_STEREOMAS
	    double cx = 339.45210732, cy = 272.93266689; // PROSILICA_STEREOMAS
		double focal = 0.5*(fx+fy);
		cv::Mat cam_intrinsic_matrix = (cv::Mat_<float>(3, 3) << fx, 0, cx, 0, fy, cy, 0, 0, 1);

		cv::Mat maskE_inliers; 
		// switch to RANSAC from LMED
		essential_matrix_ = cv::findEssentialMat(img1_pts, img2_pts, focal, cv::Point2d(cx, cy), cv::RANSAC, 0.999, 3.0, maskE_inliers);
		int num_inliers = cv::recoverPose(essential_matrix_, img1_pts, img2_pts, R_, t_, focal, cv::Point2d(cx, cy), maskE_inliers); // returns inliers that pass cheirality check

		for(int i = 0 ; i < maskE_inliers.rows; i++) {
			if (maskE_inliers.at<bool>(i,0) == 1) {
				matches_global_robust_.push_back(matches_global_[i]);
			}
		}

		// visualizeFeatureMatches(matches_global_);
		// visualizeFeatureMatches(matches_global_robust_);

		std::cout << R_ << std::endl;
		std::cout << t_ << std::endl;
	}
	else {
		std::cout<<"Insufficient features!\n";
	}
}
