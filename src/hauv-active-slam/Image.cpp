#include "Image.hpp"

Image::Image(const bot_core::image_t *message, const bool cam_id) {

  // TODO : Setup Image Constructor for simulated camera data
  // if (use_simulator) {
    // SetupSimulatedImage(message);
    // return;
  // }

  utime_ = static_cast<double>(message->utime);
  utime_ /= 1e6;

  cam_id_ = cam_id; // 0 : PROSILICA_STEREOMAS, 1 : PROSILICA_STEREOSLV

  int bpp; // bytes per pixel
  if (message->pixelformat == 1497715271) { // PIXEL_FORMAT_GRAY = 1497715271
      bpp = 1; 
  }
  else if (message->pixelformat == 909199180) { // PIXEL_FORMAT_LE_GRAY16 = 909199180
      bpp = 2;
  }
  else if (message->pixelformat == 859981650) { // PIXEL_FORMAT_RGB = 859981650
      bpp = 3;
  }
  else {
      std::cerr << "ERROR: unknown image format" << std::endl;
      exit(1);
  }

  img_width_ = message->width; // cols
  img_height_ = message->height; // rows
  uint8_t *img_data_arr = (uint8_t*) malloc (img_width_*img_height_*bpp*sizeof (uint8_t));
  std::copy(message->data.begin(), message->data.end(), img_data_arr);
  img_raw_ = cv::Mat(img_height_, img_width_, CV_16UC1, img_data_arr);

  img_raw_.convertTo(img_raw_, CV_8UC1, 0.00390625);
  img_rect_ = rectifyImage();

}

cv::Mat Image::getRawImage() {
  return img_raw_;
}

double Image::getUtime() {
  return utime_;
}

cv::Mat Image::getRectifiedImage() {
  return img_rect_;
}

void Image::setFrameID(int frame_id) { 
  frame_id_ = frame_id;
}

cv::Mat Image::rectifyImage() {
  double fx, fy, cx, cy; 
  double k_arr[5]; 

  if (cam_id_ == 1) { // PROSILICA_STEREOSLV
    fx = 844.10245182;
    fy = 841.12174071;
    cx = 336.16770975;
    cy = 280.22260368;

    k_arr[0] = 0.1416890046668197;
    k_arr[1] = 0.509762179513644;
    k_arr[2] = 0.004237115216087523;
    k_arr[3] = 0.001244190065673104;
    k_arr[4] = 0;
  }

  else { // PROSILICA_STEREOMAS
    fx = 843.13619593;
    fy = 840.04059281;
    cx = 339.45210732;
    cy = 272.93266689;

    k_arr[0] = 0.1543360056146553;
    k_arr[1] = 0.4456057115710337;
    k_arr[2] = -0.0004516777522099794;
    k_arr[3] = 0.001987414749648615;
    k_arr[4] = 0;
  }

  cv::Mat M = (cv::Mat_<float>(3, 3) << fx, 0, cx, 0, fy, cy, 0, 0, 1); // camera intrinsics matrix
  cv::Mat D = (cv::Mat_<float>(5, 1) << k_arr[0], k_arr[1], k_arr[2], k_arr[3], k_arr[4]); // distortion coefficients

  // TODEBUG : Initializing from double array k_arr does not initialize cv::Mat D correctly
  // cv::Mat D = cv::Mat(5, 1, CV_32F, k_arr); // distortion coefficients

  cv::Mat img_rect;
  cv::Rect roi;
  cv::Size img_size(img_width_, img_height_);
  double alpha = 0; // 0 : all pixels in the undistorted image are valid
  cv::Mat M_new = cv::getOptimalNewCameraMatrix(M, D, img_size, alpha, img_size, &roi, true);
  cv::undistort(img_raw_, img_rect, M, D, M_new);

  return img_rect;
}


