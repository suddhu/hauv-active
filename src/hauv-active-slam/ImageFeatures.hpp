/**
 * @file Features.hpp
 * @author Paloma Sodhi (psodhi@andrew.cmu.edu)
 * @date 2017-10-26
 * @brief A class 
 */

#pragma once

#include <cstdint>
#include <iostream>
#include <vector>

#include "lcmtypes/bot_core/image_t.hpp"

#include "opencv-utils.hpp"
#include "opencv2/xfeatures2d.hpp"
#include <opencv2/features2d/features2d.hpp>

/*! \class Features Features.hpp "hauv-vision/Features.hpp"
 *  \brief Brief description.
 *
 * 
 */

class ImageFeatures
{

public:
	/*!
	  \brief Default constructor.
	 */
	ImageFeatures();

	/*!
	  \brief Constructor from a cv::Mat image
	  \param [in] message The cv::Mat image whose 2D features are computed 
	 */
	ImageFeatures(cv::Mat);

	/*!
	  \brief Default destructor.
	 */
	~ImageFeatures(){
		cv::destroyAllWindows();
	}

	void setInputImage(cv::Mat);
	
	void computeSURFFeatures();
	std::vector<cv::KeyPoint> getSURFKeypoints();
	cv::Mat getSURFDescriptor();
	cv::Mat getImageWithSURFKeypoints();

private:
    cv::Mat img_in_;

    cv::Mat surf_descriptor_; // nKeypts x nDims
	std::vector<cv::KeyPoint> surf_keypoints_;

	bool vis_flag_;
};