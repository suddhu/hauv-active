/**
 * @file Backend.hpp
 * @author Sudharshan Suresh (suddhu@cmu.edu)
 * @date 2018-05-07
 * @brief backend for upward-cam
 */

#ifndef BACKEND_H
#define BACKEND_H

#include <vector>
#include <mutex>
#include <map>

#include <gtsam/inference/Symbol.h>
#include <gtsam/geometry/Pose3.h>
#include <gtsam/slam/PriorFactor.h>
#include <gtsam/slam/BetweenFactor.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <gtsam/nonlinear/GaussNewtonOptimizer.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>
#include <gtsam/nonlinear/ISAM2.h>
#include <gtsam/nonlinear/Marginals.h>

#include <gtsam/geometry/SimpleCamera.h>
#include <gtsam/nonlinear/NonlinearFactor.h>
#include <boost/optional/optional_io.hpp>


#endif // BACKEND_H