#include "ImageFeatures.hpp"

// using namespace cv;
// using namespace cv::xfeatures2d;

ImageFeatures::ImageFeatures() {
	vis_flag_ = false;
}

ImageFeatures::ImageFeatures(cv::Mat img_in) {
	img_in_ = img_in;
	vis_flag_ = false;
}

void ImageFeatures::setInputImage(cv::Mat img_in) {
	img_in_ = img_in;
}

void ImageFeatures::computeSURFFeatures() {

	int min_hessian = 400;

	// SURF Feature class takes 8U type image input
	cv::Mat img_gray8;
	if (cvutils::imtype2str(img_in_.type()) == "16UC1")
		img_in_.convertTo(img_gray8, CV_8U, 0.00390625);
	else
		img_gray8 = img_in_;

	// Compute keypoints
	cv::Ptr<cv::Feature2D> feat2d = cv::xfeatures2d::SURF::create(min_hessian, 4, 2, true);
	feat2d->detectAndCompute(img_gray8, cv::noArray(), surf_keypoints_, surf_descriptor_);
	// std::cout << "No. of keypoints detected : " << surf_keypoints_.size() << std::endl;
	// std::cout << "Descriptor Type : " << cvutils::imtype2str(surf_descriptor_.type())  << std::endl;
	// std::cout << "Descriptor Size : " << surf_descriptor_.size()  << std::endl;

	// Visualize
	if (vis_flag_) {
		cv::Mat img_keypoints;
		cv::drawKeypoints(img_gray8, surf_keypoints_, img_keypoints, cv::Scalar::all(-1), cv::DrawMatchesFlags::DEFAULT );
		cv::imshow("Keypoints", img_keypoints);
		cv::waitKey(0);
	}

}

std::vector<cv::KeyPoint> ImageFeatures::getSURFKeypoints() {
	return surf_keypoints_;
}

cv::Mat ImageFeatures::getSURFDescriptor() {
	return surf_descriptor_;
}

cv::Mat ImageFeatures::getImageWithSURFKeypoints() {

	cv::Mat img_gray8;
	if (cvutils::imtype2str(img_in_.type()) == "16UC1")
		img_in_.convertTo(img_gray8, CV_8U, 0.00390625);
	else
		img_gray8 = img_in_;

	cv::Mat img_keypoints;
	cv::drawKeypoints(img_gray8, surf_keypoints_, img_keypoints, cv::Scalar::all(-1), cv::DrawMatchesFlags::DEFAULT );

	return img_keypoints;
}