# Introduction

This repository is meant to be cloned as a submodule within other repositories.
It contains cmake macros to simplfiy the build process.

# Compiling requirements

None.  Place this repository within a parent repository and set your CMake environment accordingly.
